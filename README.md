# Duration of year on a planet

This python project is about finding "How many days in a year" on a specific planet, by using Radius and velocity of that planet.


#OUTPUT
'Radius of the orbit (million km) : 150
Orbital speed (km/s) : 30
The duration of the year on the planet is : 364 days.
'