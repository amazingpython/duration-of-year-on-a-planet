################################################
#Calculate the duration of the year on planet
################################################

#!python 3.x

#import the constant pi from math module
from math import pi

#Input String are converted to Float number
radius = float(input("Radius of the orbit (million km): "))
velocity = float(input("Orbital speed (km/s) : "))

#Convert Millions of kilometers into just kilometers
radius = radius * 1000000

#The length of circumference = (2 * pi * R) is orbit.
#When orbit is divided by Velocity, we get year.(in seconds)
year = (2 * pi * radius) / velocity

#To Convert Seconds into days
#seconds divided by 60 = minutes,
#Minutes divided by 60 = hours
#Hours divided by 24 = days

day = round(year /  (60 * 60 * 24))

print(f"The duration of the year on planet is : {day} days.")
